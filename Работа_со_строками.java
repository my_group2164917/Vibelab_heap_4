public class Main
{
	public static void main(String[] args) {
	    int count = 0;
	    int min = -2;
	    int latin_only_count = 0;
	    boolean palindrom;
		String str = "Object-oriented programming is a programming language model organized around objects rather than \"actions\" and data rather than logic. Object-oriented programming blablalbalb. Object-oriented programming blalb.";
	    String s = "Object-oriented programming";
	    String min_unic_word = " ";
	    System.out.println("Starting string:");
	    System.out.println(str);
	    for(int i = 0;i<(str.length() - s.length());i++){
	        if(str.regionMatches(true, i,s,0,s.length())){
	            count++;
	            if(count%2 == 0){str = str.substring(0,i) + "OOP" + str.substring(i + s.length());}
	        }
	    }
	    System.out.println("-----------------");	    
	    System.out.println("Resulting string:");
	    System.out.println(str);
	    str = str.replace(".","");
	    str = str.replace(",","");
	    str = str.replace("!","");
	    str = str.replace("?","");
	    str = str.replace("\"","");
	    String[] words = str.split(" ");
	    System.out.println("------------");	    
	    System.out.println("Palindromes:");
	    for(String word: words){
	        if(word.matches("[a-zA-Z]{1,}"))latin_only_count++;
	        count = 0;
	        for(int i=0;i<word.length();i++){
	            if(word.substring(i+1).indexOf(word.charAt(i)) == -1)count++;
	        }
	        if(count < min || min == -2){
	            min = count;
	            min_unic_word = word;
	        }
	        palindrom = true;
	        for(int i=0;i<(word.length()+1)/2;i++){
	            if(word.charAt(i) != word.charAt(word.length()-1-i))palindrom = false;
	        }
	        if(palindrom)System.out.print(word + " ");
	    }
	    System.out.println("\n---------------------------");
	    System.out.println("Amount of latin only words:");
        System.out.println(latin_only_count);
        System.out.println("------------------------------------------------");
        System.out.println("Word with a minimum number of unique characters:");
	    System.out.println(min_unic_word);
	}
}
