public class Cycles{
    public static void main(String[] args) throws java.io.IOException{
        int prev = 0;
        int swap;
        int i = 0;
        int next = 1;
        System.out.println("The first 11 terms of the Fibonacci sequence (using while):");
        while(i<11){
            System.out.print(next + " ");
            swap = next + prev;
            prev = next;
            next = swap;
            i++;
        }
        System.out.println("\nThe first 11 terms of the Fibonacci sequence (using for):");
        prev = 0;
        next = 1;
        for(i=0;i<11;i++){
            System.out.print(next + " ");
            swap = next + prev;
            prev = next;
            next = swap;
        }
    }
}
