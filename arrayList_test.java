class arrayList<T> {

    private int size;
    private T[] array;

    public arrayList() {
        size = 0;
        array = (T[]) new Object[10];
    }

    public void add(T element) {
        if (size == array.length) {
            resize();
        }
        array[size++] = element;
    }

    public void add(int index, T element) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        if (size == array.length) {
            resize();
        }
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = element;
        size++;
    }

    public boolean remove(T element) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(element)) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    public T remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        T removedElement = array[index];
        System.arraycopy(array, index + 1, array, index, size - index - 1);
        array[--size] = null;
        return removedElement;
    }

    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            array[i] = null;
        }
        size = 0;
    }

    private void resize() {
        T[] newArray = (T[]) new Object[array.length * 2];
        System.arraycopy(array, 0, newArray, 0, size);
        array = newArray;
    }
}

public class arrayList_test{
    public static void main(String[] args){
        arrayList <String> Arr1 = new arrayList<>();
        Arr1.add("All");
        Arr1.add("Working");
        Arr1.add("Fine");
        Arr1.add(1, "Is");
        System.out.print("Arr1:{");
        for(int i=0;i<Arr1.size();i++){System.out.print("\"" +Arr1.get(i)+"\""); if(i<Arr1.size()-1)System.out.print(", ");}
        System.out.println("}");
        System.out.println("Arr1.size() is " + Arr1.size());
        System.out.println("Arr1.contains(\"Working\") is " + Arr1.contains("Working"));
        Arr1.remove("Working");
        System.out.print("Arr1:{");
        for(int i=0;i<Arr1.size();i++){System.out.print("\"" +Arr1.get(i)+"\""); if(i<Arr1.size()-1)System.out.print(", ");}
        System.out.println("}");
        System.out.println("Arr1.size() is " + Arr1.size());
        System.out.println("Arr1.contains(\"Working\") is " + Arr1.contains("Working"));
        Arr1.clear();
        if(Arr1.isEmpty())System.out.println("Arr1 was successfully cleared");
    }
}
