import java.util.*;

public class Work_with_stream
{
	public static void main(String[] args) {
	    Map <String, Integer> map = new HashMap<>();;
	    List <String> list;
	    String s;
	    Scanner myscan = new Scanner (System.in);
	    System.out.println("Enter your text:");
	    
	    s = myscan.nextLine();
	    s = s.toLowerCase();
	    s = s.replaceAll("[^0-9a-z ]","");
		list = Arrays.asList(s.split(" "));
		list.stream().forEach(
		    word->{
		    if(map.containsKey(word))map.put(word,map.get(word)+1);
		    else map.put(word,1);
		    }
		    );
		System.out.println("Most frequently occurring words:");
		map.entrySet().stream()
		    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed().thenComparing(Map.Entry.comparingByKey()))
		    .limit(10)
		    .forEach(System.out::println);
	}
	
}