import java.util.Scanner;
class MyException extends Exception {
        MyException() {
        }

        MyException(String msg) {
            super(msg);
        }
    }
public class Work_with_exceptions{
    public static void main(String[] args) throws MyException{
        String s;
		Scanner myscan = new Scanner (System.in);
		System.out.println("Enter your text:");
		while((s = myscan.next())!=null){
		    try{
		    if(s.length() > 10) throw new MyException("is more then ten!");
		    }catch(MyException e){
		        System.out.println("Length of word \"" + s +"\" " + e.getMessage());
		    }
		}
    }
}