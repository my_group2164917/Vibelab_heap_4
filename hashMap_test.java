import java.util.*;
class hashMap<K, V> {

    private static final int INITIAL_CAPACITY = 16;
    private int size;
    private Node<K, V>[] table;
    public hashMap() {
        table = new Node[INITIAL_CAPACITY];
    }

    public void put(K key, V value) {
        int index = indexForKey(key);
        Node<K, V> node = table[index];
        while (node != null) {
            if (key.equals(node.key)) {
                node.value = value;
                return;
            }
            node = node.next;
        }
        
        table[index] = new Node<>(key, value, table[index]);
        size++;

        if (size >= table.length) {
            resize();
        }
    }

    public V get(K key) {
        int index = indexForKey(key);
        Node<K, V> node = table[index];
        while (node != null) {
            if (key.equals(node.key)) {
                return node.value;
            }
            node = node.next;
        }
        return null;
    }

    public Set<K> keySet() {
        Set<K> keys = new HashSet<>();
        for (Node<K, V> node : table) {
            while (node != null) {
                keys.add(node.key);
                node = node.next;
            }
        }
        return keys;
    }

    public Collection<V> values() {
        List<V> values = new ArrayList<>();
        for (Node<K, V> node : table) {
            while (node != null) {
                values.add(node.value);
                node = node.next;
            }
        }
        return values;
    }

    public boolean containsKey(K key) {
        int index = indexForKey(key);
        Node<K, V> node = table[index];
        while (node != null) {
            if (key.equals(node.key)) {
                return true;
            }
            node = node.next;
        }
        return false;
    }

    public boolean containsValue(V value) {
        for (Node<K, V> node : table) {
            while (node != null) {
                if (value.equals(node.value)) {
                    return true;
                }
                node = node.next;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public V remove(K key) {
        int index = indexForKey(key);
        Node<K, V> prev = null;
        Node<K, V> node = table[index];

        while (node != null) {
            if (key.equals(node.key)) {
                if (prev == null) {
                    table[index] = node.next;
                } else {
                    prev.next = node.next;
                }
                size--;
                return node.value;
            }
            prev = node;
            node = node.next;
        }
        return null;
    }

    public int size() {
        return size;
    }

    public void clear() {
        Arrays.fill(table, null);
        size = 0;
    }

    private int indexForKey(K key) {
        int hash = key.hashCode() % table.length;
        return hash < 0 ? hash + table.length : hash;
    }

    private void resize() {
        Node<K, V>[] newTable = new Node[table.length * 2];
        System.arraycopy(table, 0, newTable, 0, table.length);
        table = newTable;
    }

    private static class Node<K, V> {
        K key;
        V value;
        Node<K, V> next;

        public Node(K key, V value, Node<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
public class hashMap_test{
    public static void main(String[] args) {
        hashMap<String, Integer> map = new hashMap<>();
        map.put("One", 1);
        map.put("Two", 2);
        map.put("Three", 3);

        System.out.println("Size: " + map.size());
        System.out.println("Value of key 'One': " + map.get("One"));
        System.out.println("Contains key 'Two': " + map.containsKey("Two"));
        System.out.println("Contains value 3: " + map.containsValue(3));
        System.out.println("Keys: " + map.keySet());
        System.out.println("Values: " + map.values());
        System.out.println("map.remove(\"Three\");");
        map.remove("Three");
        System.out.println("Keys: " + map.keySet());
        System.out.println("Values: " + map.values());
        System.out.println("Size after remove: " + map.size());
        System.out.println("map.clear();");
        map.clear();
        if(map.isEmpty())System.out.println("map was successfully cleared");
    }
}