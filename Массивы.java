public class Main {
    public static void main(String[] args){
        int[] numbers = new int[8];
        int prev = 0;
        boolean increasing = true; 
        System.out.println("Initial array:");
        for(int i=0;i<8;i++){
            numbers[i] = (int)(Math.random() * 10 + 1);
            System.out.print(numbers[i] + " ");
            if(prev >= numbers[i])increasing = false;
            if(i%2 == 1)numbers[i] = 0;
            prev = numbers[i];
        }
        if(increasing){
            System.out.println("\nThis array is strictly increasing:");
            }else {System.out.println("\nThis array is not strictly increasing:");}
        System.out.println("Final array:");
        for(int num: numbers){System.out.print(num + " ");}
    }
}
