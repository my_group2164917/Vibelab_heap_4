class Product
{
    String name;
    int price;
    int rating;
    Product(String n, int p, int r){
        name = n;
        price = p;
        rating = r;
    }
}
class Category
{
    String name;
    Product[] products;
    Category(String n, Product[] p){
        name = n;
        products = p;
    }
    void DisplayInfo(){
        System.out.println("Products in the category(" + name + "):");
        for(Product p: products){System.out.print(p.name + " ");}
        System.out.println();
    }
}
class Basket
{
    Product[] purchased_products;
    Basket(Product[] p){purchased_products = p;}
    void DisplayInfo(String name){
        System.out.println("Products in the " + name + "'s basket:");
        for(Product p: purchased_products){System.out.print(p.name + " ");}
        System.out.println();
    }
}
class User
{
    String login;
    String password;
    Basket basket;
    User(String l, String p, Basket b){
        login = l;
        password = p;
        basket = b;
    }
    void DisplayInfo(){
        System.out.println("User's information:");
        System.out.println("Login: " + login);
        System.out.println("Password: " + password);
        basket.DisplayInfo(login);
    }
}
public class OOP
{
	public static void main(String[] args) {
		Product[] products1 = {new Product("Onion", 16, 56), new Product("Tomato", 24, 46),new Product("Cucumber", 34, 67)};
		Product[] products2 = {new Product("Apple", 26, 89), new Product("Pear", 74, 36),new Product("Banana", 38, 57)};
		Category vegetables = new Category("Vegetables", products1);
		Category fruits = new Category("Fruits", products2);
		User Vasya = new User("Vasiliy","vasya123",new Basket(products1));
		vegetables.DisplayInfo();
		fruits.DisplayInfo();
		Vasya.DisplayInfo();
	}
}
