import java.util.Scanner;

class WrongFormatException extends Exception {
        WrongFormatException() {
        }

        WrongFormatException(String msg) {
            super(msg);
        }
    }

public class Work_with_time{
    public static boolean isLeap(int year){
        boolean fl;
        if(year % 4 == 0){
            if(year % 100 == 0){
                if(year % 400 == 0){
                    fl = true;
                }else fl = false;
            }else fl = true;
        }else fl = false;
        return fl;
    }
    public static void main(String[] args) throws WrongFormatException{
        String s;
        int year;
        System.out.println("Enter the date in the dd.MM.yyyy format");
		Scanner myscan = new Scanner (System.in);
		try{
		s = myscan.nextLine();
		if(s.matches("(0?[1-9]|[12][0-9]|3[01]).(0?[1-9]|1[012]).([0-9]{4})") == false)throw new WrongFormatException("Format is wrong!");
		}catch(WrongFormatException e){
		    System.out.println(e.getMessage());
		    return;
		}
        year = Integer.parseInt(s.substring(6,10));
        System.out.print(year);
        if(isLeap(year))System.out.print(" is leap year!");
        else System.out.print(" isn't leap year!");
    }
}