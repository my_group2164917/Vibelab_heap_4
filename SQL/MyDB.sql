CREATE DATABASE MyDB;
-- create
USE MyDB;
CREATE TABLE Fines(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    carNumber VARCHAR(255),
    violatorFullName VARCHAR(255),
    trafficOfficerFullName VARCHAR(255),
    protocolAuthor VARCHAR(255),
    protocolTime VARCHAR(255),
    fineAmount DOUBLE,
    isSummons BOOLEAN,
    isPaid BOOLEAN,
    paymentDate VARCHAR(255),
    deadlineDate VARCHAR(255)
    
);

-- insert
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("a123aa","Èâàíîâ Èâàí Èâàíîâè÷", "Ïåòðîâ Ïåòð Ïåòðîâè÷", "Ñèäîðîâà Àííà Âëàäèìèðîâíà", "10:30 01.01.2021", 5000, true, true, "02.01.2021", "31.01.2021");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("e456pc","Ñìèðíîâà Åëåíà Àëåêñàíäðîâíà", "Êóçíåöîâ Èëüÿ Àíäðååâè÷", "Èâàíîâà Îëüãà Ïåòðîâíà", "15:45 05.02.2021", 3000, false, false, "-", "06.03.2021");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("k789py","Ïåòðîâ Äìèòðèé Âëàäèìèðîâè÷", "Ñîêîëîâà Àíàñòàñèÿ Ñåðãååâíà", "Êóçíåöîâà Åêàòåðèíà Àëåêñàíäðîâíà", "08:20 10.03.2021", 10000, true, false, "-", "11.04.2021");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("m147oo","Êîçëîâà Ìàðèÿ Ñåðãååâíà", "Èâàíîâ Èãîðü Àëåêñàíäðîâè÷", "Ñìèðíîâà Åêàòåðèíà Äìèòðèåâíà", "12:15 15.05.2021", 7000, true, true, "16.05.2021", "14.06.2021");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("t258ek","Ñîêîëîâ Àíäðåé Âèêòîðîâè÷", "Êóçíåöîâà Þëèÿ Ñåðãååâíà", "Ïåòðîâ Àëåêñåé Âëàäèìèðîâè÷", "09:50 20.06.2021", 2000, false, true, "21.06.2021", "20.07.2021");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("y369xa","Êóçíåöîâ Äåíèñ Âëàäèìèðîâè÷", "Èâàíîâà Àííà Âèêòîðîâíà", "Ñìèðíîâ Èãîðü Àëåêñàíäðîâè÷", "13:30 25.08.2021", 4000, false, false, "-", "26.09.2021");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("p951nu","Ñèäîðîâ Èëüÿ Àíäðååâè÷", "Ïåòðîâà Åêàòåðèíà Äìèòðèåâíà", "Êîçëîâ Äìèòðèé Âëàäèìèðîâè÷", "16:00 30.10.2021", 8000, true, false, "-", "01.12.2021");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("o753mi","Èâàíîâà Îëüãà Àëåêñàíäðîâíà", "Ñîêîëîâ Èãîðü Âèêòîðîâè÷", "Êóçíåöîâ Äåíèñ Âëàäèìèðîâè÷", "11:20 05.12.2021", 1500, false, true, "06.12.2021", "05.01.2022");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("n246ux", "Ïåòðîâà Þëèÿ Ñåðãååâíà", "Êîçëîâ Àíäðåé Âèêòîðîâè÷", "Ñèäîðîâ Èëüÿ Àíäðååâè÷", "14:10 10.02.2022", 6000, true, true, "11.02.2022", "12.03.2022");
INSERT INTO Fines(carNumber, violatorFullName, trafficOfficerFullName, protocolAuthor, protocolTime, fineAmount, isSummons, isPaid, paymentDate, deadlineDate) 
		VALUES ("c852va", "Ñìèðíîâ Èãîðü Àëåêñàíäðîâè÷", "Ñèäîðîâ Èâàí Àíäðååâè÷", "Èâàíîâà Îëüãà Àëåêñàíäðîâíà", "17:30 15.04.2022", 9000, false, false, "-", "16.05.2022");