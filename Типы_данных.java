public class Main {
    public static void main(String[] args) {
        int studentId = 0x385A0; // номер зачетной книжки в шестнадцатеричном формате
        long phoneNumber = 89501542490L; // номер мобильного телефона в десятичном формате
        byte lastTwoDigits = 0b110001; // последние две ненулевые цифры номера телефона в двоичном формате
        short lastFourDigits = 010231; // последние четыре ненулевые цифры номера телефона в восьмеричном формате
        int result = (studentId%100 - 1) % 26 + 1; // увеличенное на единицу значение остатка от деления на 26 уменьшенного на единицу номера студента в журнале группы
        char alphabetLetter = (char) ('A' + result - 1); // символ английского алфавита в верхнем регистре, номер которого соответствует найденному ранее значению
        
        System.out.println("Student ID: " + studentId);
        System.out.println("Phone number: " + phoneNumber);
        System.out.println("Last two digits: " + lastTwoDigits);
        System.out.println("Last four digits: " + lastFourDigits);
        System.out.println("Result: " + result);
        System.out.println("Alphabet letter: " + alphabetLetter);
    }
}
